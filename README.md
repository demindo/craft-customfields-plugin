# custom-fields plugin for Craft CMS 3.x

Demindo custom fields plugin

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require rveede/custom-fields

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for custom-fields.

## custom-fields Overview

-Insert text here-

## Configuring custom-fields

-Insert text here-

## Using custom-fields

-Insert text here-

## custom-fields Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Ronald van Eede](http://ronaldvaneede.me)
