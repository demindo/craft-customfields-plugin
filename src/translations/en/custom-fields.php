<?php
/**
 * custom-fields plugin for Craft CMS 3.x
 *
 * Demindo custom fields plugin
 *
 * @link      http://ronaldvaneede.me
 * @copyright Copyright (c) 2018 Ronald van Eede
 */

/**
 * custom-fields en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('custom-fields', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Ronald van Eede
 * @package   Customfields
 * @since     0.0.1
 */
return [
    'custom-fields plugin loaded' => 'custom-fields plugin loaded',
];
